# SOLAR CALCULATOR APP

### Review
Application was created as a graduation work and is still developed. 
The purpose of app is to calculate power and costs of photovoltaic installation and returns its main parameters.
During development I realised that application can be split into few parts - photovoltaic module, solar thermal collectors module and heat pump module, 
so first I wrote photovoltaic module (when I am writing this it's not completely ready yet) and started to plan and develop another modules.

App uses two formulas to calculate power of PV system (+1 be added (more complex)). 
It is important that my app is based on data which change everyday and can be different from actual conditions in your area, area and house. 

Application makes no representations or warranties about the accuracy, reliability and real costs or size of solar systems, 

...created by human for humans - so it can contains bugs and worms ;)

### Technology used
 - Java 7, 8
 - Spring 5, SpringBoot, REST
 - JavaScript, jQuery
 - HTML5, CSS3
 - PostgreSQL
 - Testing: JUnit, Mockito
 - Git, 
 - GIMP
 - Maven, log4j
 

### Build app 
```
mvn package
```

### Run Tests
```
mvn test
```

### REST API


```
POST /calculate
```
Send info from user (example below) and return response to show  

- request body
```
{
    "city": "Kraków",
    "deviationFromSouth": 10,
    "energyCost": 2000,
    "energyusage": 5000
    "roofAngle": 30,
    "roofArea":120
    "roofType": "dachówka"
}
```
- response from app
```
{
    "systemPower": 4.46372756604154,
    "systemRoofArea": 62.5,
    "energyCoverage": 1.92,
    "totalCost": 5600,
    "pvPanel": 
        {
            "id": 1,
            "name": "first panel",
            "type": "monocrystal",
            "maxPower": 200,
            "price": 200,
            "area": 2.5,
            "efficiency": 0.18
        },
    "inverter": 
        {
            "id": 1,
            "name": "Inverter One",
            "power": 350,
            "price": 600
        }
}
```
##### Other available endpoints (returns data from database):

```
/city/all
/panel/all
/inverter/all
/panel/all/asc
/inverter/all/asc
/panel/strongest
/inverter/strongest
/panel/{name}
/inverter/{name}
```








### Design overview
Solar panel calculator is based on three classes which model elements of system.
Application parses info from client to ClientParameters object which is then handle to SolarPanelService object 
which manages all of the operations and is main class of app (probably). SolarPanelService is an abstraction layer which should be used for returning response
for controllers which then pass them as a server response.

Controllers receive and send JSONs.

Application uses two formulas (for now) for calculating solar panel system's energy.
User's values validation takes place  in frontend part - using JS to prevent sending numbers like "01100", 
writing numbers which contain letters, dots or negative values of energy usage etc.


### TESTS

Tests cover logic of classes:
- InverterRepositoryImpl
- AreaCounter
- EnergyDemandCounter


### Possible improvements and extra features
- migrate Spring configuration to .xml file
- refactor methods to increase Java 8 usage (i.e. Optionals?)
- unit, integration tests
- logging to file
- improve mechanism of connections with database (we lose some time with creating queries to db)
- mapping endpoints
- add another formula for calculating energy of PV system
- add another modules (heat pump, solar thermal collectors)

###### Note
Application is developing as a graduation work on Agricultural University of Cracow.
It should helps people calculate how much do they have to spend to start producing 'green energy' and leave fossil fuels, 
should helps understand how many benefits they can gain - no production carbon dioxide, reimbursement of installation costs after about 10-15 years etc.


###### P.S.
It is my way of learning and testing technologies, solutions and improving my programming skills :D
