package com.solarcalculator.controllers.requests;

import java.math.BigDecimal;

public class ClientParameters {

    private String energyUsage;
    private String energyCost;
    private String city;
    private String roofAngle;
    private String deviationFromSouth;
    private String roofArea;
    private String roofType;

    public ClientParameters(){}

    public ClientParameters(String energyUsage, String energyCost, String city, String roofAngle, String deviationFromSouth, String roofArea, String roofType) {
        this.energyUsage = energyUsage;
        this.energyCost = energyCost;
        this.city = city;
        this.roofAngle = roofAngle;
        this.deviationFromSouth = deviationFromSouth;
        this.roofArea = roofArea;
        this.roofType = roofType;
    }


    public int getEnergyUsage() {
        return Integer.parseInt(energyUsage);
    }

    public void setEnergyUsage(String energyUsage) {
        this.energyUsage = energyUsage;
    }

    public BigDecimal getEnergyCost() {
        return BigDecimal.valueOf(Integer.parseInt(energyCost));
    }

    public void setEnergyCost(String energyCost) {
        this.energyCost = energyCost;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getRoofAngle() {
        return Integer.parseInt(roofAngle);
    }

    public void setRoofAngle(String roofAngle) {
        this.roofAngle = roofAngle;
    }

    public int getDeviationFromSouth() {
        return Integer.parseInt(deviationFromSouth);
    }

    public void setDeviationFromSouth(String deviationFromSouth) {
        this.deviationFromSouth = deviationFromSouth;
    }

    public int getRoofArea() {
        return Integer.parseInt(roofArea);
    }

    public void setRoofArea(String roofArea) {
        this.roofArea = roofArea;
    }

    public String getRoofType() {
        return roofType;
    }

    public void setRoofType(String roofType) {
        this.roofType = roofType;
    }
}


