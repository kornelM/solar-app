package com.solarcalculator.controllers.sc_controllers.mainControllers;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class MainController {


    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index() {
        return "indexPL";
    }

    @RequestMapping(value = "/about", method = RequestMethod.GET)
    public String getAboutMe() {
        return "about";
    }

    @RequestMapping(value = "/how_it_works", method = RequestMethod.GET)
    public String getHowItWorsk() {
        return "howItWorks";
    }
}
