package com.solarcalculator.controllers.sc_controllers.mainControllers;


import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.websocket.OnError;

@Controller
public class MyErrorController implements ErrorController {


    private static final String PATH = "/error";

    @RequestMapping(value = PATH)
    public String onError() {
        return "errorPagePL";
    }
    @Override
    public String getErrorPath() {
        return PATH;
    }


    
}
