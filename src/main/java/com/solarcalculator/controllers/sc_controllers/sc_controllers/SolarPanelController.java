package com.solarcalculator.controllers.sc_controllers.sc_controllers;


import com.solarcalculator.controllers.requests.ClientParameters;
import com.solarcalculator.model.ResponseObject;
import com.solarcalculator.model.database.SolarPanelRepositoryImpl;
import com.solarcalculator.model.services.SolarPanelService;
import com.solarcalculator.model.services.factors.CorrectionFactor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


// CALCULATIONS AND HANDLING REQUESTS FROM CALCULATOR


@Controller
public class SolarPanelController {

    private SolarPanelService solarPanelService;

    @Autowired
    public SolarPanelController(SolarPanelService solarPanelService) {
        this.solarPanelService = solarPanelService;
    }

    @RequestMapping(value = "/calculate", method = RequestMethod.POST)
    public ResponseEntity<ResponseObject> mainPage(@RequestBody ClientParameters clientParameters) {
        ResponseObject response = solarPanelService.calculateSystem(clientParameters);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
