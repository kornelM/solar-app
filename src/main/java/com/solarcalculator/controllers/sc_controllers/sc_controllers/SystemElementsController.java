package com.solarcalculator.controllers.sc_controllers.sc_controllers;


import com.solarcalculator.model.database.CityRepositoryImpl;
import com.solarcalculator.model.database.InverterRepositoryImpl;
import com.solarcalculator.model.database.SolarPanelRepositoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;



//ADMINISTRATOR REQUESTS

@Controller
public class SystemElementsController {

    private CityRepositoryImpl cityRepository;
    private SolarPanelRepositoryImpl solarPanelRepository;
    private InverterRepositoryImpl inverterRepository;

    @Autowired
    public SystemElementsController(CityRepositoryImpl cityRepository, SolarPanelRepositoryImpl solarPanelRepository, InverterRepositoryImpl inverterRepository) {
        this.cityRepository = cityRepository;
        this.solarPanelRepository = solarPanelRepository;
        this.inverterRepository = inverterRepository;
    }


    @RequestMapping (value = "/city/all", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity getAllCities (){
        return new ResponseEntity<>(cityRepository.getAll(), HttpStatus.OK);
    }

    @RequestMapping (value = "/panel/all", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity getAllPvPanels (){
        return new ResponseEntity<>(solarPanelRepository.getAll(), HttpStatus.OK);
    }

    @RequestMapping (value = "/inverter/all", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity getAllInverters (){
        return new ResponseEntity<>(inverterRepository.getAll(), HttpStatus.OK);
    }

    @RequestMapping (value = "/panel/all/asc", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity getAllPvPanelsAsc (){
        return new ResponseEntity<>(solarPanelRepository.getAllAsc(), HttpStatus.OK);
    }

    @RequestMapping (value = "panel/strongest", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity getTheStrongestPv (){
        return new ResponseEntity<>(solarPanelRepository.getTheStrongest(), HttpStatus.OK);
    }

    @RequestMapping (value = "/inverter/all/asc", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity getAllInvertersAsc (){
        return new ResponseEntity<>(inverterRepository.getAllByPowerAsc(), HttpStatus.OK);
    }

    @RequestMapping (value = "/inverter/strongest", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity getTheStrongestInv (){
        return new ResponseEntity<>(inverterRepository.getTheStrongest(), HttpStatus.OK);
    }

    @RequestMapping (value = "/panel/{name}", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity getPanelByName (@PathVariable("name") String name){
        return new ResponseEntity<>(solarPanelRepository.findByName(name), HttpStatus.OK);
    }

    @RequestMapping (value = "/inverter/{name}", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity getInvByName (@PathVariable("name") String name){
        return new ResponseEntity<>(inverterRepository.findByName(name), HttpStatus.OK);
    }
}
