package com.solarcalculator.model;

import com.solarcalculator.model.database.database_pojos.Inverter;
import com.solarcalculator.model.database.database_pojos.PvPanel;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;


@Component
public class ResponseObject {
    private double systemPower;
    private double systemRoofArea;
    private double energyCoverage;
    private BigDecimal totalCost;
    private PvPanel pvPanel;
    private Inverter inverter;


    public ResponseObject(){
        totalCost = BigDecimal.valueOf(0.0);
    }


    public void addToTotalCost (double additionalCost) {
        totalCost.add(BigDecimal.valueOf(additionalCost));
    }

    public void addToTotalCost (BigDecimal additionalCost) {
        totalCost = (additionalCost.add(totalCost));
    }


    public PvPanel getPvPanel() {
        return pvPanel;
    }

    public void setPvPanel(PvPanel pvPanel) {
        this.pvPanel = pvPanel;
    }

    public double getSystemPower() {
        return systemPower;
    }

    public void setSystemPower(double systemPower) {
        this.systemPower = systemPower;
    }

    public double getSystemRoofArea() {
        return systemRoofArea;
    }

    public void setSystemRoofArea(double systemRoofArea) {
        this.systemRoofArea = systemRoofArea;
    }

    public double getEnergyCoverage() {
        return energyCoverage;
    }

    public void setEnergyCoverage(double energyCoverage) {
        this.energyCoverage = energyCoverage;
    }

    public BigDecimal getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(BigDecimal totalCost) {
        this.totalCost = totalCost;
    }

    public Inverter getInverter() {
        return inverter;
    }

    public void setInverter(Inverter inverter) {
        this.inverter = inverter;
    }
}
