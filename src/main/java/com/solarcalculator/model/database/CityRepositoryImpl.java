package com.solarcalculator.model.database;

import com.solarcalculator.model.database.database_pojos.City;
import com.solarcalculator.model.interfaces.ICityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CityRepositoryImpl {

    private ICityRepository iCityRepository;

    @Autowired
    public CityRepositoryImpl(ICityRepository iCityRepository) {
        this.iCityRepository = iCityRepository;
    }

    public List<City> getAll() {
        return iCityRepository.findAll();
    }

    public City findByName (String cityName){
        return iCityRepository.findByName(cityName);
    }


}
