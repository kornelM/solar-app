package com.solarcalculator.model.database;

import com.solarcalculator.model.database.database_pojos.Inverter;
import com.solarcalculator.model.interfaces.IInverterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class InverterRepositoryImpl {


    private IInverterRepository iInverterRepository;


    @Autowired
    public InverterRepositoryImpl(IInverterRepository iInverterRepository) {
        this.iInverterRepository = iInverterRepository;
    }

    public List<Inverter> getAll () {
        return iInverterRepository.findAll();
    }

    public List<Inverter> getAllByPowerAsc(){
        return iInverterRepository.getInverterByPowerAsc();
    }

    public Inverter findByName(String name){
        return iInverterRepository.findInverterByName(name);
    }

    public Inverter getTheStrongest() {
        return iInverterRepository.getTheStrongest();
    }


    private boolean myMethod(){
        return true;
    }
}
