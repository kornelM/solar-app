package com.solarcalculator.model.database;


import com.solarcalculator.model.database.database_pojos.PvPanel;
import com.solarcalculator.model.interfaces.ISolarPanelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SolarPanelRepositoryImpl {

    private ISolarPanelRepository iSolarPanelRepository;

    @Autowired
    public SolarPanelRepositoryImpl(ISolarPanelRepository iSolarPanelRepository) {
        this.iSolarPanelRepository = iSolarPanelRepository;
    }

    public List<PvPanel> getAll() {
        return iSolarPanelRepository.findAll();
    }

    public PvPanel findByName (String cityName){
        return iSolarPanelRepository.findPanelPvByName(cityName);
    }

    public List<PvPanel> getAllAsc() {
        return iSolarPanelRepository.getPvPanelByMaxPowerAsc();
    }

    public PvPanel getTheStrongest() {
        return iSolarPanelRepository.getStrongest();
    }

}
