package com.solarcalculator.model.database.database_pojos;

import javax.persistence.*;

@Entity
@Table (name = "sc_city_details")
public class City {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "insolation_kwhs")
    private int insolation;

    @Column(name = "sunny_hours")
    private int sunnyHours;

    public City(){}

    public City(String name, int insolation, int sunnyHours) {
        this.name = name;
        this.insolation = insolation;
        this.sunnyHours = sunnyHours;
    }




    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getInsolation() {
        return insolation;
    }

    public void setInsolation(int insolation) {
        this.insolation = insolation;
    }

    public int getSunnyHours() {
        return sunnyHours;
    }

    public void setSunnyHours(int sunnyHours) {
        this.sunnyHours = sunnyHours;
    }

    @Override
    public String toString() {
        return "City{" +
                "name='" + name + '\'' +
                ", insolation=" + insolation +
                ", sunnyHours=" + sunnyHours +
                '}';
    }
}
