package com.solarcalculator.model.database.database_pojos;


import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "sc_inverter_details")
public class Inverter {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "power")
    private int power;

    @Column(name = "price")
    private BigDecimal price;

    public Inverter() {
    }

    public Inverter(String name, int power, BigDecimal price) {
        this.name = name;
        this.power = power;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }


    @Override
    public String toString() {
        return "Inverter{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", power=" + power +
                ", price=" + price +
                '}';
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (this == null){
            return false;
        }
        if (this.getClass() != object.getClass()){
            return false;
        }

        Inverter inv = (Inverter) object;


        if (this.getPower() == inv.getPower() &&
                this.getName().equals(inv.getName()) &&
                this.getPrice().equals(inv.getPrice()) ) {
            return true;
        } else {
            return false;
        }
    }

}
