package com.solarcalculator.model.database.database_pojos;


import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "sc_panel_details")
public class PvPanel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "type")
    private String type;

    @Column(name = "max_power")
    private int maxPower;

    @Column(name = "price")
    private BigDecimal price;

    @Column(name = "area")
    private double area;

    @Column(name = "efficiency")
    private double efficiency;

    public PvPanel() {
    }

    public PvPanel(String name, String type, int maxPower, BigDecimal price, double area, double efficiency) {
        this.name = name;
        this.type = type;
        this.maxPower = maxPower;
        this.price = price;
        this.area = area;
        this.efficiency = efficiency;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getMaxPower() {
        return maxPower;
    }

    public void setMaxPower(int maxPower) {
        this.maxPower = maxPower;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = BigDecimal.valueOf(price);
    }

    public double getArea() {
        return area;
    }

    public void setArea(float area) {
        this.area = area;
    }

    public double getEfficiency() {
        return efficiency;
    }

    public void setEfficiency(float efficiency) {
        this.efficiency = efficiency;
    }

    @Override
    public String toString() {
        return "PvPanel{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", type='" + type + '\'' +
                ", maxPower=" + maxPower +
                ", price=" + price +
                ", area=" + area +
                ", efficiency=" + efficiency +
                '}';
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (this == null) {
            return false;
        }
        if (this.getClass() != object.getClass()) {
            return false;
        }

        PvPanel pvp = (PvPanel) object;


        if (this.getName().equals(pvp.getName()) &&
                this.getArea() == (pvp.getArea()) &&
                this.getMaxPower() == pvp.getMaxPower() &&
                this.getPrice().equals(pvp.getPrice()) &&
                this.getEfficiency() == pvp.getEfficiency() &&
                this.getType().equals(pvp.getType()) &&
                this.getId() == pvp.getId()) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = name.hashCode();
        result = 31 * result + type.hashCode();
        result = 31 * result + maxPower;
        result = 31 * result + price.hashCode();
        temp = Double.doubleToLongBits(area);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(efficiency);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}
