package com.solarcalculator.model.interfaces;

import com.solarcalculator.model.database.database_pojos.City;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


public interface ICityRepository extends JpaRepository<City, Integer> {

    @Query("SELECT C FROM City C WHERE C.name = :cityName")
    City findByName(@Param("cityName") String nameOfCity);
}
