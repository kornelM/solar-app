package com.solarcalculator.model.interfaces;

import com.solarcalculator.model.database.database_pojos.Inverter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface IInverterRepository extends JpaRepository<Inverter, Integer> {

    @Query("SELECT I FROM Inverter I WHERE I.name = :name")
    Inverter findInverterByName(@Param("name") String nameOfInverter);

    @Query("SELECT I FROM Inverter I ORDER BY I.power ASC")
    List<Inverter> getInverterByPowerAsc();

    @Query("select I from Inverter I WHERE I.power = (select max(power) from Inverter )")
    Inverter getTheStrongest();
}
