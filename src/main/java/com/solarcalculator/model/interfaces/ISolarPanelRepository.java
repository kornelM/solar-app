package com.solarcalculator.model.interfaces;


import com.solarcalculator.model.database.database_pojos.PvPanel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ISolarPanelRepository extends JpaRepository <PvPanel, Integer> {

    @Query("SELECT P FROM PvPanel P WHERE P.name = :name")
    public PvPanel findPanelPvByName(@Param("name") String pvPanelName);

    @Query("SELECT P FROM PvPanel P ORDER BY P.maxPower ASC")
    public List<PvPanel> getPvPanelByMaxPowerAsc();

    @Query ("select P from PvPanel P WHERE P.maxPower = (select max(maxPower) from PvPanel )")
    public PvPanel getStrongest();

//    @Query ("select P from PvPanel P")

}
