package com.solarcalculator.model.services;

import com.solarcalculator.controllers.requests.ClientParameters;
import com.solarcalculator.model.ResponseObject;
import com.solarcalculator.model.database.CityRepositoryImpl;
import com.solarcalculator.model.database.InverterRepositoryImpl;
import com.solarcalculator.model.database.SolarPanelRepositoryImpl;
import com.solarcalculator.model.database.database_pojos.City;
import com.solarcalculator.model.database.database_pojos.Inverter;
import com.solarcalculator.model.database.database_pojos.PvPanel;
import com.solarcalculator.model.services.counters.AreaCounter;
import com.solarcalculator.model.services.counters.EnergyDemandCounter;
import com.solarcalculator.model.services.factors.CorrectionFactor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Service
public class SolarPanelService {

    private AreaCounter areaCounter;
    private EnergyDemandCounter energyDemandCounter;
    private SolarPanelRepositoryImpl solarPanelRepository;
    private CityRepositoryImpl cityRepository;
    private InverterRepositoryImpl inverterRepository;


    @Autowired
    public SolarPanelService(AreaCounter areaCounter, EnergyDemandCounter energyDemandCounter,
                             SolarPanelRepositoryImpl solarPanelRepository, CityRepositoryImpl cityRepository,
                             InverterRepositoryImpl inverterRepository) {
        this.areaCounter = areaCounter;
        this.energyDemandCounter = energyDemandCounter;
        this.solarPanelRepository = solarPanelRepository;
        this.cityRepository = cityRepository;
        this.inverterRepository = inverterRepository;
    }

    public ResponseObject calculateSystem(ClientParameters cp) {
        ResponseObject response = new ResponseObject();
        City city = cityRepository.findByName(cp.getCity());
        List<PvPanel> pvPanels = solarPanelRepository.getAllAsc();
        List<Inverter> inverterList = inverterRepository.getAllByPowerAsc();

        PvPanel responsePvPanel = areaCounter.choosePanel(cp.getEnergyUsage(), cp.getRoofArea(), pvPanels);
        response.setPvPanel(responsePvPanel);

        int numberOfPvPanels = areaCounter.determineNumOfPanels(cp.getEnergyUsage(), responsePvPanel.getMaxPower());
        double areaForSolarPanels = areaCounter.countAreaForPanels(numberOfPvPanels, response.getPvPanel());
        response.setSystemRoofArea(areaForSolarPanels);
        response.setEnergyCoverage(areaCounter.evaluateCoverage(cp.getRoofArea(), areaForSolarPanels));

        response.setSystemPower(energyDemandCounter.determineSystemEnergy(cp.getEnergyUsage(), city));

        Inverter chosenInverter = energyDemandCounter.selectInverter(cp.getEnergyUsage(), inverterList);
        response.setInverter(chosenInverter);


        response.addToTotalCost(chosenInverter.getPrice());
        response.addToTotalCost(calculatePanelsCost(numberOfPvPanels, response.getPvPanel()));

        return response;
    }


    public BigDecimal calculatePanelsCost(int numOfPanels, PvPanel pvPanel) {
        return pvPanel.getPrice().multiply(BigDecimal.valueOf(numOfPanels));
    }

}
