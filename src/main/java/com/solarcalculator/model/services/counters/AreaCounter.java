package com.solarcalculator.model.services.counters;


import com.solarcalculator.model.database.SolarPanelRepositoryImpl;
import com.solarcalculator.model.database.database_pojos.PvPanel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Service
public class AreaCounter {
    private double areaForPanels;
    private int roofArea;
    private int numberOfPanels;
    private PvPanel pvPanel;
    private double coverage;

    private SolarPanelRepositoryImpl solarPanelRepository;


    @Autowired
    public AreaCounter(SolarPanelRepositoryImpl solarPanelRepository) {
        this.solarPanelRepository = solarPanelRepository;
    }

    public PvPanel choosePanel(double energyDemandWh, int roofArea, List<PvPanel> pvPanelList) {
        double areaOfCurrent;
        double areaOfPrevious;

        BigDecimal priceCurrent;
        BigDecimal pricePrevious;

        BigDecimal totalPriceCurrent;
        BigDecimal totalPricePrevious;


        PvPanel responsePanel = pvPanelList.get(0);

        for (int i = 1; i < pvPanelList.size(); i++) {
            areaOfCurrent = determineNumOfPanels(energyDemandWh, pvPanelList.get(i).getMaxPower());
            areaOfPrevious = determineNumOfPanels(energyDemandWh, responsePanel.getMaxPower());

            priceCurrent = pvPanelList.get(i).getPrice();
            pricePrevious = responsePanel.getPrice();

            totalPriceCurrent = priceCurrent.multiply(BigDecimal.valueOf(areaOfCurrent));
            totalPricePrevious = pricePrevious.multiply(BigDecimal.valueOf(areaOfPrevious));

            if (totalPriceCurrent.compareTo(totalPricePrevious) < 1) {
                responsePanel = pvPanelList.get(i);
            }

        }
        return responsePanel;
    }

    public double countAreaForPanels(double numOfPanels, PvPanel pvPanel) {
        return numOfPanels * pvPanel.getArea();
    }

    public void count(double energyDemand, int roofArea, List<PvPanel> pvPanelList) {
        int numOfPanels;
        double countedAreaForPanels;
        this.roofArea = roofArea;
        for (PvPanel p : pvPanelList) {
            numOfPanels = determineNumOfPanels(energyDemand, p.getMaxPower()); // licze ilosc paneli
            countedAreaForPanels = countAreaForPanels(numOfPanels, p); // liczę potrzebną dla nich powierzchnię

            if (isRoofAreaHigherThanPanels(roofArea, countedAreaForPanels)) { // jeśli klient ma wystarczająco powierzchni
                this.areaForPanels = countedAreaForPanels;
                this.numberOfPanels = numOfPanels;
                this.pvPanel = p;
                this.coverage = 1;
            } else {                // if client does NOT has enough area for panels to cover 100% energy demand
                this.pvPanel = solarPanelRepository.getTheStrongest();
                this.numberOfPanels = determineNumOfPanels(energyDemand, pvPanel.getMaxPower());
                this.areaForPanels = countAreaForPanels(numberOfPanels, pvPanel);
                this.coverage = evaluateCoverage(roofArea, areaForPanels);
            }
        }
    }

    public int determineNumOfPanels(double energyDemandWh, double powerOfPanel) {
        return (int) Math.ceil(energyDemandWh / powerOfPanel);
    }

    private boolean isRoofAreaHigherThanPanels(int roofArea, double countedAreaForPanels) {
        return roofArea > countedAreaForPanels;
    }

    public double evaluateCoverage(int roofArea, double panelsArea) {
        return roofArea / panelsArea;
    }
}
