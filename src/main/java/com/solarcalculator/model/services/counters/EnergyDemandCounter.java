package com.solarcalculator.model.services.counters;


import com.solarcalculator.model.database.CityRepositoryImpl;
import com.solarcalculator.model.database.InverterRepositoryImpl;
import com.solarcalculator.model.database.database_pojos.City;
import com.solarcalculator.model.database.database_pojos.Inverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;

@Service
public class EnergyDemandCounter {

    private final double CORRECTION_FACTOR = 0.9;
    private final double CONSTANT_MAX = 0.9;
    private final double CONSTANT_MIN = 0.5;
    private final int PRODUCTION = 950;


    @Autowired
    public EnergyDemandCounter() {}


    public double determineSystemEnergy(int clientEnergyUsageWh, City city) {
        int insolation = city.getInsolation();
        double f1 = systemPowerFirstFormula(clientEnergyUsageWh);
        double f2 = systemPowerSecondFormula(clientEnergyUsageWh, insolation);

        return countAverage(f1, f2);
    }

    public double systemPowerFirstFormula(int clientEnergyUsageWh) {
        return clientEnergyUsageWh * CORRECTION_FACTOR / PRODUCTION;
    }

    public double systemPowerSecondFormula(int clientEnergyUsageWh, int insolation) {
        double efficiency = 0.8;
        double systemPowerMax = CONSTANT_MAX * clientEnergyUsageWh  / (insolation * efficiency);
        double systemPowerMin = CONSTANT_MIN * clientEnergyUsageWh  / (insolation * efficiency);

        return countAverage(systemPowerMax, systemPowerMin);
    }

    private double countAverage(double... args) {
        double result = 0;
        for (double d : args) {
            result += d;
        }

        return result / args.length;
    }

    public Inverter selectInverter(double systemEnergyWh, List<Inverter> inverterList) {
        for (Inverter i : inverterList) {
            if (i.getPower() > systemEnergyWh) {
                return i;
            }
        }
        return inverterList.get(inverterList.size() - 1);
    }

    private Inverter findTheStrongest(List<Inverter> inverterList) {
        return inverterList.stream().max(Comparator.comparingInt(Inverter::getPower)).orElse(null);
    }
}
