package com.solarcalculator.model.services.factors;

import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;


@Component
public class CorrectionFactor {
    private static Map correctionFactors;

    private class MyKey {

        private int key1;
        private int key2;

        MyKey(int key1, int key2) {
            this.key1 = key1;
            this.key2 = key2;
        }

        public int getKey1() {
            return key1;
        }

        public void setKey1(int key1) {
            this.key1 = key1;
        }

        public int getKey2() {
            return key2;
        }

        public void setKey2(int key2) {
            this.key2 = key2;
        }

        @Override
        public boolean equals(Object o) {
            if (o == this) return true;
            if (!(o instanceof MyKey)) {
                return false;
            }

            MyKey myKey = (MyKey) o;

            return myKey.key1 == key1 && myKey.key2 == key2;
        }

        @Override
        public int hashCode() {
            int hashCode = 0;
            hashCode = 31 * hashCode + Integer.valueOf(key1).hashCode();
            hashCode = 31 * hashCode + Integer.valueOf(key2).hashCode();
            return hashCode;
        }
    }

    public CorrectionFactor() {
        initMap();
    }

    public double getFactor(int ra, int dfs) {
        MyKey tmpKey = new MyKey(ra, dfs);
        return (double) correctionFactors.get(tmpKey);
    }

    private void initMap() {
        //first parameter is roof angle, second deviation from south = value is correction factor
        correctionFactors = new HashMap<MyKey, Double>();
        correctionFactors.put(new MyKey(0, 0), 1.00);
        correctionFactors.put(new MyKey(5, 0), 1.04);
        correctionFactors.put(new MyKey(10, 0), 1.07);
        correctionFactors.put(new MyKey(15, 0), 1.10);
        correctionFactors.put(new MyKey(20, 0), 1.10);
        correctionFactors.put(new MyKey(25, 0), 1.12);
        correctionFactors.put(new MyKey(30, 0), 1.13);
        correctionFactors.put(new MyKey(35, 0), 1.13);
        correctionFactors.put(new MyKey(40, 0), 1.13);
        correctionFactors.put(new MyKey(45, 0), 1.12);
        correctionFactors.put(new MyKey(50, 0), 1.11);
        correctionFactors.put(new MyKey(55, 0), 1.08);
        correctionFactors.put(new MyKey(60, 0), 1.06);
        correctionFactors.put(new MyKey(65, 0), 1.02);
        correctionFactors.put(new MyKey(70, 0), 0.99);
        correctionFactors.put(new MyKey(75, 0), 0.95);
        correctionFactors.put(new MyKey(80, 0), 0.90);
        correctionFactors.put(new MyKey(85, 0), 0.85);
        correctionFactors.put(new MyKey(90, 0), 0.80);


        correctionFactors.put(new MyKey(0, 5), 1.00);
        correctionFactors.put(new MyKey(5, 5), 1.04);
        correctionFactors.put(new MyKey(10, 5), 1.07);
        correctionFactors.put(new MyKey(15, 5), 1.09);
        correctionFactors.put(new MyKey(20, 5), 1.11);
        correctionFactors.put(new MyKey(25, 5), 1.12);
        correctionFactors.put(new MyKey(30, 5), 1.13);
        correctionFactors.put(new MyKey(35, 5), 1.13);
        correctionFactors.put(new MyKey(40, 5), 1.13);
        correctionFactors.put(new MyKey(45, 5), 1.12);
        correctionFactors.put(new MyKey(50, 5), 1.10);
        correctionFactors.put(new MyKey(55, 5), 1.08);
        correctionFactors.put(new MyKey(60, 5), 1.06);
        correctionFactors.put(new MyKey(65, 5), 1.02);
        correctionFactors.put(new MyKey(70, 5), 0.99);
        correctionFactors.put(new MyKey(75, 5), 0.95);
        correctionFactors.put(new MyKey(80, 5), 0.90);
        correctionFactors.put(new MyKey(85, 5), 0.85);
        correctionFactors.put(new MyKey(90, 5), 0.80);

        correctionFactors.put(new MyKey(0, 10), 1.00);
        correctionFactors.put(new MyKey(5, 10), 1.04);
        correctionFactors.put(new MyKey(10, 10), 1.07);
        correctionFactors.put(new MyKey(15, 10), 1.09);
        correctionFactors.put(new MyKey(20, 10), 1.11);
        correctionFactors.put(new MyKey(25, 10), 1.12);
        correctionFactors.put(new MyKey(30, 10), 1.13);
        correctionFactors.put(new MyKey(35, 10), 1.13);
        correctionFactors.put(new MyKey(40, 10), 1.13);
        correctionFactors.put(new MyKey(45, 10), 1.12);
        correctionFactors.put(new MyKey(50, 10), 1.10);
        correctionFactors.put(new MyKey(55, 10), 1.08);
        correctionFactors.put(new MyKey(60, 10), 1.05);
        correctionFactors.put(new MyKey(65, 10), 1.02);
        correctionFactors.put(new MyKey(70, 10), 0.99);
        correctionFactors.put(new MyKey(75, 10), 0.94);
        correctionFactors.put(new MyKey(80, 10), 0.90);
        correctionFactors.put(new MyKey(85, 10), 0.85);
        correctionFactors.put(new MyKey(90, 10), 0.80);


        correctionFactors.put(new MyKey(0, 15), 1.00);
        correctionFactors.put(new MyKey(5, 15), 1.03);
        correctionFactors.put(new MyKey(10, 15), 1.06);
        correctionFactors.put(new MyKey(15, 15), 1.09);
        correctionFactors.put(new MyKey(20, 15), 1.11);
        correctionFactors.put(new MyKey(25, 15), 1.12);
        correctionFactors.put(new MyKey(30, 15), 1.13);
        correctionFactors.put(new MyKey(35, 15), 1.13);
        correctionFactors.put(new MyKey(40, 15), 1.12);
        correctionFactors.put(new MyKey(45, 15), 1.11);
        correctionFactors.put(new MyKey(50, 15), 1.10);
        correctionFactors.put(new MyKey(55, 15), 1.08);
        correctionFactors.put(new MyKey(60, 15), 1.05);
        correctionFactors.put(new MyKey(65, 15), 1.02);
        correctionFactors.put(new MyKey(70, 15), 0.98);
        correctionFactors.put(new MyKey(75, 15), 0.94);
        correctionFactors.put(new MyKey(80, 15), 0.90);
        correctionFactors.put(new MyKey(85, 15), 0.85);
        correctionFactors.put(new MyKey(90, 15), 0.80);

        correctionFactors.put(new MyKey(0, 20), 1.00);
        correctionFactors.put(new MyKey(5, 20), 1.03);
        correctionFactors.put(new MyKey(10, 20), 1.06);
        correctionFactors.put(new MyKey(15, 20), 1.09);
        correctionFactors.put(new MyKey(20, 20), 1.10);
        correctionFactors.put(new MyKey(25, 20), 1.12);
        correctionFactors.put(new MyKey(30, 20), 1.12);
        correctionFactors.put(new MyKey(35, 20), 1.12);
        correctionFactors.put(new MyKey(40, 20), 1.12);
        correctionFactors.put(new MyKey(45, 20), 1.11);
        correctionFactors.put(new MyKey(50, 20), 1.09);
        correctionFactors.put(new MyKey(55, 20), 1.07);
        correctionFactors.put(new MyKey(60, 20), 1.05);
        correctionFactors.put(new MyKey(65, 20), 1.02);
        correctionFactors.put(new MyKey(70, 20), 0.98);
        correctionFactors.put(new MyKey(75, 20), 0.94);
        correctionFactors.put(new MyKey(80, 20), 0.89);
        correctionFactors.put(new MyKey(85, 20), 0.85);
        correctionFactors.put(new MyKey(90, 20), 0.80);


        correctionFactors.put(new MyKey(0, 25), 1.00);
        correctionFactors.put(new MyKey(5, 25), 1.03);
        correctionFactors.put(new MyKey(10, 25), 1.06);
        correctionFactors.put(new MyKey(15, 25), 1.08);
        correctionFactors.put(new MyKey(20, 25), 1.10);
        correctionFactors.put(new MyKey(25, 25), 1.11);
        correctionFactors.put(new MyKey(30, 25), 1.12);
        correctionFactors.put(new MyKey(35, 25), 1.12);
        correctionFactors.put(new MyKey(40, 25), 1.11);
        correctionFactors.put(new MyKey(45, 25), 1.10);
        correctionFactors.put(new MyKey(50, 25), 1.09);
        correctionFactors.put(new MyKey(55, 25), 1.07);
        correctionFactors.put(new MyKey(60, 25), 1.04);
        correctionFactors.put(new MyKey(65, 25), 1.01);
        correctionFactors.put(new MyKey(70, 25), 0.97);
        correctionFactors.put(new MyKey(75, 25), 0.93);
        correctionFactors.put(new MyKey(80, 25), 0.89);
        correctionFactors.put(new MyKey(85, 25), 0.84);
        correctionFactors.put(new MyKey(90, 25), 0.79);


        correctionFactors.put(new MyKey(0, 30), 1.00);
        correctionFactors.put(new MyKey(5, 30), 1.03);
        correctionFactors.put(new MyKey(10, 30), 1.06);
        correctionFactors.put(new MyKey(15, 30), 1.08);
        correctionFactors.put(new MyKey(20, 30), 1.09);
        correctionFactors.put(new MyKey(25, 30), 1.10);
        correctionFactors.put(new MyKey(30, 30), 1.11);
        correctionFactors.put(new MyKey(35, 30), 1.11);
        correctionFactors.put(new MyKey(40, 30), 1.10);
        correctionFactors.put(new MyKey(45, 30), 1.10);
        correctionFactors.put(new MyKey(50, 30), 1.08);
        correctionFactors.put(new MyKey(55, 30), 1.06);
        correctionFactors.put(new MyKey(60, 30), 1.03);
        correctionFactors.put(new MyKey(65, 30), 1.00);
        correctionFactors.put(new MyKey(70, 30), 0.97);
        correctionFactors.put(new MyKey(75, 30), 0.93);
        correctionFactors.put(new MyKey(80, 30), 0.89);
        correctionFactors.put(new MyKey(85, 30), 0.84);
        correctionFactors.put(new MyKey(90, 30), 0.79);


        correctionFactors.put(new MyKey(0, 35), 1.00);
        correctionFactors.put(new MyKey(5, 35), 1.03);
        correctionFactors.put(new MyKey(10, 35), 1.05);
        correctionFactors.put(new MyKey(15, 35), 1.07);
        correctionFactors.put(new MyKey(20, 35), 1.09);
        correctionFactors.put(new MyKey(25, 35), 1.10);
        correctionFactors.put(new MyKey(30, 35), 1.10);
        correctionFactors.put(new MyKey(35, 35), 1.10);
        correctionFactors.put(new MyKey(40, 35), 1.10);
        correctionFactors.put(new MyKey(45, 35), 1.09);
        correctionFactors.put(new MyKey(50, 35), 1.07);
        correctionFactors.put(new MyKey(55, 35), 1.05);
        correctionFactors.put(new MyKey(60, 35), 1.02);
        correctionFactors.put(new MyKey(65, 35), 0.99);
        correctionFactors.put(new MyKey(70, 35), 0.96);
        correctionFactors.put(new MyKey(75, 35), 0.92);
        correctionFactors.put(new MyKey(80, 35), 0.88);
        correctionFactors.put(new MyKey(85, 35), 0.83);
        correctionFactors.put(new MyKey(90, 35), 0.79);


        correctionFactors.put(new MyKey(0, 40), 1.00);
        correctionFactors.put(new MyKey(5, 40), 1.03);
        correctionFactors.put(new MyKey(10, 40), 1.05);
        correctionFactors.put(new MyKey(15, 40), 1.07);
        correctionFactors.put(new MyKey(20, 40), 1.08);
        correctionFactors.put(new MyKey(25, 40), 1.09);
        correctionFactors.put(new MyKey(30, 40), 1.09);
        correctionFactors.put(new MyKey(35, 40), 1.09);
        correctionFactors.put(new MyKey(40, 40), 1.09);
        correctionFactors.put(new MyKey(45, 40), 1.07);
        correctionFactors.put(new MyKey(50, 40), 1.06);
        correctionFactors.put(new MyKey(55, 40), 1.04);
        correctionFactors.put(new MyKey(60, 40), 1.01);
        correctionFactors.put(new MyKey(65, 40), 0.98);
        correctionFactors.put(new MyKey(70, 40), 0.95);
        correctionFactors.put(new MyKey(75, 40), 0.91);
        correctionFactors.put(new MyKey(80, 40), 0.87);
        correctionFactors.put(new MyKey(85, 40), 0.83);
        correctionFactors.put(new MyKey(90, 40), 0.79);


        correctionFactors.put(new MyKey(0, 45), 1.00);
        correctionFactors.put(new MyKey(5, 45), 1.03);
        correctionFactors.put(new MyKey(10, 45), 1.05);
        correctionFactors.put(new MyKey(15, 45), 1.06);
        correctionFactors.put(new MyKey(20, 45), 1.07);
        correctionFactors.put(new MyKey(25, 45), 1.08);
        correctionFactors.put(new MyKey(30, 45), 1.08);
        correctionFactors.put(new MyKey(35, 45), 1.08);
        correctionFactors.put(new MyKey(40, 45), 1.07);
        correctionFactors.put(new MyKey(45, 45), 1.06);
        correctionFactors.put(new MyKey(50, 45), 1.04);
        correctionFactors.put(new MyKey(55, 45), 1.02);
        correctionFactors.put(new MyKey(60, 45), 1.00);
        correctionFactors.put(new MyKey(65, 45), 0.97);
        correctionFactors.put(new MyKey(70, 45), 0.93);
        correctionFactors.put(new MyKey(75, 45), 0.90);
        correctionFactors.put(new MyKey(80, 45), 0.86);
        correctionFactors.put(new MyKey(85, 45), 0.82);
        correctionFactors.put(new MyKey(90, 45), 0.77);


        correctionFactors.put(new MyKey(0, 50), 1.00);
        correctionFactors.put(new MyKey(5, 50), 1.02);
        correctionFactors.put(new MyKey(10, 50), 1.04);
        correctionFactors.put(new MyKey(15, 50), 1.05);
        correctionFactors.put(new MyKey(20, 50), 1.06);
        correctionFactors.put(new MyKey(25, 50), 1.07);
        correctionFactors.put(new MyKey(30, 50), 1.07);
        correctionFactors.put(new MyKey(35, 50), 1.07);
        correctionFactors.put(new MyKey(40, 50), 1.06);
        correctionFactors.put(new MyKey(45, 50), 1.05);
        correctionFactors.put(new MyKey(50, 50), 1.03);
        correctionFactors.put(new MyKey(55, 50), 1.01);
        correctionFactors.put(new MyKey(60, 50), 0.98);
        correctionFactors.put(new MyKey(65, 50), 0.95);
        correctionFactors.put(new MyKey(70, 50), 0.92);
        correctionFactors.put(new MyKey(75, 50), 0.89);
        correctionFactors.put(new MyKey(80, 50), 0.85);
        correctionFactors.put(new MyKey(85, 50), 0.81);
        correctionFactors.put(new MyKey(90, 50), 0.76);


        correctionFactors.put(new MyKey(0, 55), 1.00);
        correctionFactors.put(new MyKey(5, 55), 1.02);
        correctionFactors.put(new MyKey(10, 55), 1.04);
        correctionFactors.put(new MyKey(15, 55), 1.05);
        correctionFactors.put(new MyKey(20, 55), 1.05);
        correctionFactors.put(new MyKey(25, 55), 1.06);
        correctionFactors.put(new MyKey(30, 55), 1.06);
        correctionFactors.put(new MyKey(35, 55), 1.05);
        correctionFactors.put(new MyKey(40, 55), 1.04);
        correctionFactors.put(new MyKey(45, 55), 1.03);
        correctionFactors.put(new MyKey(50, 55), 1.01);
        correctionFactors.put(new MyKey(55, 55), 0.99);
        correctionFactors.put(new MyKey(60, 55), 0.96);
        correctionFactors.put(new MyKey(65, 55), 0.94);
        correctionFactors.put(new MyKey(70, 55), 0.91);
        correctionFactors.put(new MyKey(75, 55), 0.87);
        correctionFactors.put(new MyKey(80, 55), 0.83);
        correctionFactors.put(new MyKey(85, 55), 0.79);
        correctionFactors.put(new MyKey(90, 55), 0.75);


        correctionFactors.put(new MyKey(0, 60), 1.00);
        correctionFactors.put(new MyKey(5, 60), 1.02);
        correctionFactors.put(new MyKey(10, 60), 1.03);
        correctionFactors.put(new MyKey(15, 60), 1.04);
        correctionFactors.put(new MyKey(20, 60), 1.04);
        correctionFactors.put(new MyKey(25, 60), 1.05);
        correctionFactors.put(new MyKey(30, 60), 1.04);
        correctionFactors.put(new MyKey(35, 60), 1.04);
        correctionFactors.put(new MyKey(40, 60), 1.03);
        correctionFactors.put(new MyKey(45, 60), 1.01);
        correctionFactors.put(new MyKey(50, 60), 1.00);
        correctionFactors.put(new MyKey(55, 60), 0.97);
        correctionFactors.put(new MyKey(60, 60), 0.95);
        correctionFactors.put(new MyKey(65, 60), 0.92);
        correctionFactors.put(new MyKey(70, 60), 0.89);
        correctionFactors.put(new MyKey(75, 60), 0.86);
        correctionFactors.put(new MyKey(80, 60), 0.82);
        correctionFactors.put(new MyKey(85, 60), 0.78);
        correctionFactors.put(new MyKey(90, 60), 0.74);


        correctionFactors.put(new MyKey(0, 65), 1.00);
        correctionFactors.put(new MyKey(5, 65), 1.01);
        correctionFactors.put(new MyKey(10, 65), 1.02);
        correctionFactors.put(new MyKey(15, 65), 1.03);
        correctionFactors.put(new MyKey(20, 65), 1.03);
        correctionFactors.put(new MyKey(25, 65), 1.03);
        correctionFactors.put(new MyKey(30, 65), 1.03);
        correctionFactors.put(new MyKey(35, 65), 1.02);
        correctionFactors.put(new MyKey(40, 65), 1.01);
        correctionFactors.put(new MyKey(45, 65), 1.00);
        correctionFactors.put(new MyKey(50, 65), 0.98);
        correctionFactors.put(new MyKey(55, 65), 0.96);
        correctionFactors.put(new MyKey(60, 65), 0.93);
        correctionFactors.put(new MyKey(65, 65), 0.90);
        correctionFactors.put(new MyKey(70, 65), 0.87);
        correctionFactors.put(new MyKey(75, 65), 0.84);
        correctionFactors.put(new MyKey(80, 65), 0.80);
        correctionFactors.put(new MyKey(85, 65), 0.77);
        correctionFactors.put(new MyKey(90, 65), 0.72);


        correctionFactors.put(new MyKey(0, 70), 1.00);
        correctionFactors.put(new MyKey(5, 70), 1.01);
        correctionFactors.put(new MyKey(10, 70), 1.02);
        correctionFactors.put(new MyKey(15, 70), 1.02);
        correctionFactors.put(new MyKey(20, 70), 1.02);
        correctionFactors.put(new MyKey(25, 70), 1.02);
        correctionFactors.put(new MyKey(30, 70), 1.01);
        correctionFactors.put(new MyKey(35, 70), 1.00);
        correctionFactors.put(new MyKey(40, 70), 0.99);
        correctionFactors.put(new MyKey(45, 70), 0.98);
        correctionFactors.put(new MyKey(50, 70), 0.96);
        correctionFactors.put(new MyKey(55, 70), 0.94);
        correctionFactors.put(new MyKey(60, 70), 0.91);
        correctionFactors.put(new MyKey(65, 70), 0.88);
        correctionFactors.put(new MyKey(70, 70), 0.85);
        correctionFactors.put(new MyKey(75, 70), 0.82);
        correctionFactors.put(new MyKey(80, 70), 0.79);
        correctionFactors.put(new MyKey(85, 70), 0.75);
        correctionFactors.put(new MyKey(90, 70), 0.71);


        correctionFactors.put(new MyKey(0, 75), 1.00);
        correctionFactors.put(new MyKey(5, 75), 1.01);
        correctionFactors.put(new MyKey(10, 75), 1.01);
        correctionFactors.put(new MyKey(15, 75), 1.01);
        correctionFactors.put(new MyKey(20, 75), 1.01);
        correctionFactors.put(new MyKey(25, 75), 1.00);
        correctionFactors.put(new MyKey(30, 75), 1.00);
        correctionFactors.put(new MyKey(35, 75), 0.99);
        correctionFactors.put(new MyKey(40, 75), 0.97);
        correctionFactors.put(new MyKey(45, 75), 0.96);
        correctionFactors.put(new MyKey(50, 75), 0.94);
        correctionFactors.put(new MyKey(55, 75), 0.92);
        correctionFactors.put(new MyKey(60, 75), 0.89);
        correctionFactors.put(new MyKey(65, 75), 0.85);
        correctionFactors.put(new MyKey(70, 75), 0.83);
        correctionFactors.put(new MyKey(75, 75), 0.80);
        correctionFactors.put(new MyKey(80, 75), 0.77);
        correctionFactors.put(new MyKey(85, 75), 0.73);
        correctionFactors.put(new MyKey(90, 75), 0.69);


        correctionFactors.put(new MyKey(0, 80), 1.00);
        correctionFactors.put(new MyKey(5, 80), 1.00);
        correctionFactors.put(new MyKey(10, 80), 1.01);
        correctionFactors.put(new MyKey(15, 80), 1.00);
        correctionFactors.put(new MyKey(20, 80), 1.00);
        correctionFactors.put(new MyKey(25, 80), 0.99);
        correctionFactors.put(new MyKey(30, 80), 0.98);
        correctionFactors.put(new MyKey(35, 80), 0.97);
        correctionFactors.put(new MyKey(40, 80), 0.95);
        correctionFactors.put(new MyKey(45, 80), 0.93);
        correctionFactors.put(new MyKey(50, 80), 0.92);
        correctionFactors.put(new MyKey(55, 80), 0.89);
        correctionFactors.put(new MyKey(60, 80), 0.87);
        correctionFactors.put(new MyKey(65, 80), 0.84);
        correctionFactors.put(new MyKey(70, 80), 0.81);
        correctionFactors.put(new MyKey(75, 80), 0.78);
        correctionFactors.put(new MyKey(80, 80), 0.75);
        correctionFactors.put(new MyKey(85, 80), 0.71);
        correctionFactors.put(new MyKey(90, 80), 0.68);


        correctionFactors.put(new MyKey(0, 85), 1.00);
        correctionFactors.put(new MyKey(5, 85), 1.00);
        correctionFactors.put(new MyKey(10, 85), 1.00);
        correctionFactors.put(new MyKey(15, 85), 0.99);
        correctionFactors.put(new MyKey(20, 85), 0.98);
        correctionFactors.put(new MyKey(25, 85), 0.97);
        correctionFactors.put(new MyKey(30, 85), 0.96);
        correctionFactors.put(new MyKey(35, 85), 0.95);
        correctionFactors.put(new MyKey(40, 85), 0.93);
        correctionFactors.put(new MyKey(45, 85), 0.91);
        correctionFactors.put(new MyKey(50, 85), 0.89);
        correctionFactors.put(new MyKey(55, 85), 0.87);
        correctionFactors.put(new MyKey(60, 85), 0.85);
        correctionFactors.put(new MyKey(65, 85), 0.82);
        correctionFactors.put(new MyKey(70, 85), 0.79);
        correctionFactors.put(new MyKey(75, 85), 0.76);
        correctionFactors.put(new MyKey(80, 85), 0.73);
        correctionFactors.put(new MyKey(85, 85), 0.69);
        correctionFactors.put(new MyKey(90, 85), 0.66);

        correctionFactors.put(new MyKey(0, 90), 1.00);
        correctionFactors.put(new MyKey(5, 90), 1.00);
        correctionFactors.put(new MyKey(10, 90), 0.99);
        correctionFactors.put(new MyKey(15, 90), 0.98);
        correctionFactors.put(new MyKey(20, 90), 0.97);
        correctionFactors.put(new MyKey(25, 90), 0.96);
        correctionFactors.put(new MyKey(30, 90), 0.94);
        correctionFactors.put(new MyKey(35, 90), 0.93);
        correctionFactors.put(new MyKey(40, 90), 0.91);
        correctionFactors.put(new MyKey(45, 90), 0.88);
        correctionFactors.put(new MyKey(50, 90), 0.87);
        correctionFactors.put(new MyKey(55, 90), 0.85);
        correctionFactors.put(new MyKey(60, 90), 0.82);
        correctionFactors.put(new MyKey(65, 90), 0.80);
        correctionFactors.put(new MyKey(70, 90), 0.77);
        correctionFactors.put(new MyKey(75, 90), 0.74);
        correctionFactors.put(new MyKey(80, 90), 0.71);
        correctionFactors.put(new MyKey(85, 90), 0.67);
        correctionFactors.put(new MyKey(90, 90), 0.64);


    }


//    private void initTable()
//                //horizontal -> deviation from south
//                //vertical -> roof angle                                deviation FROM SOUTH
//                //       90     85    80    75    70    65    60    55    50    45    40    35    30    25    20    15    10     5     0
//           /*0 ->0deg*/ {1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00},
//           /*1*/        {1.00, 1.00, 1.00, 1.01, 1.01, 1.01, 1.02, 1.02, 1.02, 1.03, 1.03, 1.03, 1.03, 1.03, 1.03, 1.03, 1.04, 1.04, 1.04},
//           /*2->10deg*/ {0.99, 1.00, 1.01, 1.01, 1.02, 1.02, 1.03, 1.04, 1.04, 1.05, 1.05, 1.05, 1.06, 1.06, 1.06, 1.06, 1.07, 1.07, 1.07},
//           /*3*/        {0.98, 0.99, 1.00, 1.01, 1.02, 1.03, 1.04, 1.05, 1.05, 1.06, 1.07, 1.07, 1.08, 1.08, 1.09, 1.09, 1.09, 1.09, 1.10},
//           /*4->20deg*/ {0.97, 0.98, 1.00, 1.01, 1.02, 1.03, 1.04, 1.05, 1.06, 1.07, 1.08, 1.09, 1.09, 1.10, 1.10, 1.11, 1.11, 1.11, 1.10},
//           /*5*/        {0.96, 0.97, 0.99, 1.00, 1.02, 1.03, 1.05, 1.06, 1.07, 1.08, 1.09, 1.10, 1.10, 1.11, 1.12, 1.12, 1.12, 1.12, 1.12},
//           /*6->30deg*/ {0.94, 0.96, 0.98, 1.00, 1.01, 1.03, 1.04, 1.06, 1.07, 1.08, 1.09, 1.10, 1.11, 1.12, 1.12, 1.13, 1.13, 1.13, 1.13},
//           /*7*/        {0.93, 0.95, 0.97, 0.99, 1.00, 1.02, 1.04, 1.05, 1.07, 1.08, 1.09, 1.10, 1.11, 1.12, 1.12, 1.13, 1.13, 1.13, 1.13},
//           /*8->40deg*/ {0.91, 0.93, 0.95, 0.97, 0.99, 1.01, 1.03, 1.04, 1.06, 1.07, 1.09, 1.10, 1.10, 1.11, 1.12, 1.12, 1.13, 1.13, 1.13},
//           /*9*/        {0.88, 0.91, 0.93, 0.96, 0.98, 1.00, 1.01, 1.03, 1.05, 1.06, 1.07, 1.09, 1.10, 1.10, 1.11, 1.11, 1.12, 1.12, 1.12},
//           /*10->50deg*/{0.87, 0.89, 0.92, 0.94, 0.96, 0.98, 1.00, 1.01, 1.03, 1.04, 1.06, 1.07, 1.08, 1.09, 1.09, 1.10, 1.10, 1.10, 1.11},       // ROOF ANGLE
//           /*11*/       {0.85, 0.87, 0.89, 0.92, 0.94, 0.96, 0.97, 0.99, 1.01, 1.02, 1.04, 1.05, 1.06, 1.07, 1.07, 1.08, 1.08, 1.08, 1.08},
//           /*12->60deg*/{0.82, 0.85, 0.87, 0.89, 0.91, 0.93, 0.95, 0.96, 0.98, 1.00, 1.01, 1.02, 1.03, 1.04, 1.05, 1.05, 1.05, 1.06, 1.06},
//           /*13*/       {0.80, 0.82, 0.84, 0.85, 0.88, 0.90, 0.92, 0.94, 0.95, 0.97, 0.98, 0.99, 1.00, 1.01, 1.02, 1.02, 1.02, 1.02, 1.02},
//           /*14->70deg*/{0.77, 0.79, 0.81, 0.83, 0.85, 0.87, 0.89, 0.91, 0.92, 0.93, 0.95, 0.96, 0.97, 0.97, 0.98, 0.98, 0.99, 0.99, 0.99},
//           /*15*/       {0.74, 0.76, 0.78, 0.80, 0.82, 0.84, 0.86, 0.87, 0.89, 0.90, 0.91, 0.92, 0.93, 0.93, 0.94, 0.94, 0.94, 0.95, 0.95},
//           /*16->80deg*/{0.71, 0.73, 0.75, 0.77, 0.79, 0.80, 0.82, 0.83, 0.85, 0.86, 0.87, 0.88, 0.89, 0.89, 0.89, 0.90, 0.90, 0.90, 0.90},
//           /*17*/       {0.67, 0.69, 0.71, 0.73, 0.75, 0.77, 0.78, 0.79, 0.81, 0.82, 0.83, 0.83, 0.84, 0.84, 0.85, 0.85, 0.85, 0.85, 0.85},
//           /*18->90deg*/{0.64, 0.66, 0.68, 0.69, 0.71, 0.72, 0.74, 0.75, 0.76, 0.77, 0.79, 0.79, 0.79, 0.79, 0.80, 0.80, 0.80, 0.80, 0.80},
//        };
//    }


}
