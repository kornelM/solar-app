$(document).ready(function () {

// ********  INIT VARIABLES **********

    let inputIds = ["energy-usage", "energy-cost", "roof-area", "roof-type", "city"];
    let fieldsDisabledCharacters = ["energy-usage", "energy-cost", "roof-area"];

    // ******** CHANGE COLOR OF INPUT TO ORANGE WHEN MARKED AS WRONG (WITH RED BOARDER)
    for (let i = 0; i < inputIds.length; i++) {
        $('#' + inputIds[i]).on('click', function (e) {
            e.preventDefault();
            $(this).css('border', '2px solid #e67e22');
            $(this).css('backgroundColor', '#cccccc');
        });

    }

    // ********* PREVENT e, =, ,, -, . IN INPUT FIELD *********
    // ********* PREVENT 0 (ZERO) AS THE FIRST DIGIT OF NUMBER IN INPUT FIELD *********
    for (let i = 0; i < fieldsDisabledCharacters.length; i++) {
        document.getElementById(fieldsDisabledCharacters[i])
            .addEventListener("keydown", function (e) {
                if ([69, 187, 188, 189, 190].includes(e.keyCode)) {
                    e.preventDefault();
                }
                if (this.value.length === 0 && e.keyCode === 48) {
                    e.preventDefault();
                }
            })
    }

    // ******** SUBMIT BUTTON LISTENER *******
    $("#submit-form-btn").click(function (event) {
        event.preventDefault();

        const formData = {
            energyUsage: $("#energy-usage").val(),
            energyCost: $("#energy-cost").val(),
            city: $("#city").val(),
            roofAngle: $("#roof-angle").val(),
            deviationFromSouth: $("#deviation-from-south").val(),
            roofArea: $("#roof-area").val() ==="" ?0 : $("#roof-area").val(),
            roofType: $("#roof-type").val()
        };


        validateInputData(formData);
    });


    function postData(data) {
        console.log(data);
        $.ajax({
            url: "/calculate",
            type: "POST",
            data: JSON.stringify(data),
            contentType: "application/json",
            success: function (data, status, jqXHR) {
                console.log("received data: " + JSON.stringify(data));
                console.log("---------------------------");
                setResults(data);
            }
        });
    }

    function validateInputData(formDataValues) {
        let inputNames = ["energy-usage", "energy-cost", "city", "roof-area", "roof-type"];
        let allInputsRight = true;

        let tmpVar = 0;
        var roofArea = document.getElementById('roof-area');
        if (roofArea.value === "" || roofArea === undefined){
            inputNames[3] = 0;
        }
        for (let i = 0; i < inputNames.length; i++) {
            tmpVar = $("#" + inputNames[i]).val();

            if (tmpVar === 0 || tmpVar === '' || tmpVar === "Wybierz miasto" || tmpVar === "choose roof") {
                changeClass(inputNames[i]);
                allInputsRight = false;
            }
        }
        if (allInputsRight) {
            postData(formDataValues);
        }
    }

    function changeClass(inputFieldId) {
        document.getElementById(inputFieldId).style.border = "2px solid #e62222";
        document.getElementById(inputFieldId).style.backgroundColor = "#bf8f8f";
    }

    function changeHiddenArtribute() {
        $('.result-section').slideToggle(500);
    }

    function setResults(data) {
        // $(".results-class").slideToggle(1500);
        changeHiddenArtribute();

        console.log(JSON.stringify(data.systemPower));
        console.log(JSON.stringify(data.systemRoofArea));
        console.log(JSON.stringify(data.totalCost));
        console.log(JSON.stringify(data.inverter.name));

        document.getElementById("system-power-span").textContent = Number(data.systemPower).toFixed(2);
        document.getElementById("system-roof-area-span").textContent = Number(data.systemRoofArea).toFixed(2);
        document.getElementById("energy-coverage-span").textContent = data.energyCoverage === 0 ? "Not available" : Number(data.energyCoverage * 100).toFixed(0) + " %";
        document.getElementById("total-costs-span").textContent = Number(data.totalCost).toFixed(2);
        document.getElementById("pv-panel-span").textContent = data.pvPanel.name;
        document.getElementById("chosen-inverter-span").textContent = data.inverter.name;
    }

})
;