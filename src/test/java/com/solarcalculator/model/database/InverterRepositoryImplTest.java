package com.solarcalculator.model.database;

import com.solarcalculator.model.database.database_pojos.Inverter;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class InverterRepositoryImplTest {


    @Test
    public void getTheStrongest() throws Exception {

        InverterRepositoryImpl inverterRepository = mock(InverterRepositoryImpl.class);
        when(inverterRepository.getTheStrongest()).thenReturn(new Inverter("testInverter", 1000, BigDecimal.valueOf(500)));

        Inverter testInverter = new Inverter("testInverter", 1000, BigDecimal.valueOf(500));

        Assert.assertTrue("Different objects", testInverter.equals(inverterRepository.getTheStrongest()));
    }
}