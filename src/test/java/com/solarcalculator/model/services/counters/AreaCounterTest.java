package com.solarcalculator.model.services.counters;

import com.solarcalculator.model.database.SolarPanelRepositoryImpl;
import com.solarcalculator.model.database.database_pojos.PvPanel;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class AreaCounterTest {
    private static final double DELTA = 1e-15;
    private List<PvPanel> listOfTestPanels;
    private AreaCounter areaCounterMocked;

    @Mock
    private SolarPanelRepositoryImpl solarPanelRepository;


    @Before
    public void setUp() {
        listOfTestPanels = Arrays.asList(
                new PvPanel("Panel1", "monocrystal", 100, BigDecimal.valueOf(400), 2.0, 0.15),
                new PvPanel("Panel2", "monocrystal", 150, BigDecimal.valueOf(420), 2.5, 0.16),
                new PvPanel("Panel3", "policrystal", 200, BigDecimal.valueOf(600), 3.0, 0.16),
                new PvPanel("Panel4", "monocrystal", 250, BigDecimal.valueOf(550), 3.5, 0.16),
                new PvPanel("Panel5", "monocrystal", 300, BigDecimal.valueOf(690), 4.0, 0.16));
        MockitoAnnotations.initMocks(this);
        areaCounterMocked = new AreaCounter(solarPanelRepository);
    }


    @Test
    public void numOfPanels() throws Exception {
        int returnedNumOfPanels = areaCounterMocked.determineNumOfPanels(4000, 300);
        int expectedNumOfPanels = 14;

        assertEquals("Different number of panels", expectedNumOfPanels, returnedNumOfPanels);
    }

    @Test
    public void demandAreaForPanels() throws Exception {
        PvPanel tmpPanel = new PvPanel("TmpPanel", "monocrystal", 100, BigDecimal.valueOf(400), 2.0, 0.15);
        double returnedNeededArea = areaCounterMocked.countAreaForPanels(10, tmpPanel);
        double expectedArea = 20.0;

        assertEquals("Different areas", expectedArea, returnedNeededArea, DELTA);
    }

    @Test
    public void countCoverage() throws Exception {
        double returnedCoverage = areaCounterMocked.evaluateCoverage(100, 500);
        double expectedCoverage = 0.2;

        assertEquals("Different energy coverages", expectedCoverage, returnedCoverage, DELTA);
    }

    @Test
    public void shouldReturnChosenPanels() {
        PvPanel expectedPvPanel = new PvPanel("Panel4", "monocrystal", 250, BigDecimal.valueOf(550), 3.5, 0.16);
        PvPanel returnedPvPanel = areaCounterMocked.choosePanel(5000, 100, listOfTestPanels);

        expectedPvPanel.equals(returnedPvPanel);
        assertEquals("Different panels", expectedPvPanel, returnedPvPanel);
    }


}