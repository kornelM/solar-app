package com.solarcalculator.model.services.counters;

import com.solarcalculator.model.database.CityRepositoryImpl;
import com.solarcalculator.model.database.database_pojos.City;
import com.solarcalculator.model.database.database_pojos.Inverter;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class EnergyDemandCounterTest {

    private static final double DELTA_FOUR_POSITIONS = 1e-4;
    private EnergyDemandCounter energyDemandCounterMocked;
    private List<Inverter> listOfTestInverters;

    @Before
    public void setUp() {
        listOfTestInverters = Arrays.asList(
                new Inverter("Inverter1", 3000, BigDecimal.valueOf(700)),
                new Inverter("Inverter2", 3500, BigDecimal.valueOf(800)),
                new Inverter("Inverter3", 4000, BigDecimal.valueOf(900)),
                new Inverter("Inverter4", 4500, BigDecimal.valueOf(1000)),
                new Inverter("Inverter5", 5000, BigDecimal.valueOf(1200)));
        energyDemandCounterMocked = new EnergyDemandCounter();
    }

    @Test
    public void determineSystemEnergy() throws Exception {
        double expectedPower = 4.4637;
//        CityRepositoryImpl cityRepositoryMocked = mock(CityRepositoryImpl.class);
//        when(cityRepositoryMocked.findByName("Kraków")).thenReturn(new City("Kraków", 1044, 1470));
        double actualValue = energyDemandCounterMocked
                .determineSystemEnergy(5000, new City("Kraków", 1044, 1470));

        assertEquals(expectedPower, actualValue, DELTA_FOUR_POSITIONS);
    }

    @Test
    public void systemPowerFirstFormula() throws Exception {
        double expected = 4.7368;
        double result = energyDemandCounterMocked.systemPowerFirstFormula(5000);

        assertEquals(expected, result, DELTA_FOUR_POSITIONS);
    }

    @Test
    public void systemPowerSecondFormula() throws Exception {
        double expected = 4.4506;
        double result = energyDemandCounterMocked.systemPowerSecondFormula(5000, 983);

        assertEquals(expected, result, DELTA_FOUR_POSITIONS);
    }

    @Test
    public void selectInverter() throws Exception {
        Inverter expectedInverter = new Inverter("Inverter5", 5000, BigDecimal.valueOf(1200));
        Inverter actualInverter = energyDemandCounterMocked.selectInverter(4567, listOfTestInverters);

        assertEquals(expectedInverter, actualInverter);

    }


}